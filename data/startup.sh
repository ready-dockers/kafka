#!/bin/bash
trap '/usr/local/kafka/bin/kafka-server-stop.sh /usr/local/kafka/config/server.properties && /usr/local/kafka/bin/zookeeper-server-stop.sh /usr/local/kafka/config/zookeeper.properties' SIGTERM \
&& bin/zookeeper-server-start.sh config/zookeeper.properties \
& bin/kafka-server-start.sh config/server.properties