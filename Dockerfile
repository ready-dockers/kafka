FROM alpine:latest

RUN apk --update add openjdk8-jre
RUN apk add --no-cache bash

WORKDIR /usr/local
RUN wget -O kafka.tgz http://mirrors.estointernet.in/apache/kafka/2.1.0/kafka_2.11-2.1.0.tgz
RUN tar -zxf kafka.tgz
RUN rm kafka.tgz

RUN mv kafka_2.11-2.1.0 kafka

WORKDIR /usr/local/kafka

COPY ./data/startup.sh /tmp/startup.sh

#SHELL ["/bin/bash", "-c"]
RUN chmod 777 /tmp/startup.sh
ENTRYPOINT ["/tmp/startup.sh"]